# Inform 6 Unit Test

This Inform 6 library allows authors to write simple unit tests.

## Usage

Tests go in objects of class `UnitTest`, whose property `test` should make assertions. We can then run them with `UnitTestRunAll()`.

```
UnitTest "My test"
	with test [;
		AssertEqual(1 + 1, 2, "1 + 1 equals 2");
		! And as many assertions as we need in this test.
	];

[ Main;
	UnitTestRunAll();
];
```

The textual name of the test ("My test" in the above excerpt) will be displayed if an assertion fails.

If a unit test is given the attribute `concealed`, it will be skipped when running all the tests. (It can be useful if one test fails but you want to fix it later, or if a test takes a lot a time to run and you want to skip it most of the time.) To run all tests, even concealed ones, write `UnitTestRunAll(true)`.

You can also run a single test by calling its `run` property. It will return `false` if the test failed. (Additionally, the test will be given the `failed` attribute.)

## Assertions

Assertions should only be made during tests. You can write them anywhere in any routine and not only in the `test` property of a `UnitTest`, but you should make sure that the said routine is only called during a test.

Here's the full list:

- `Assert(x)`: Fails if `x` is false.
- `AssertEqual(x, y)`: Fails if `x` is different from `y`.
- `AssertNotEqual(y, y)`: Fails if `x` and `y` are the same.
- `AssertGreater(x, y)`, `AssertGreaterOrEqual(x, y)`, `AssertLess(x, y)`, `AssertLessOrEqual(x, y)`: Fail if `x` is less than or equal to `y`, and so on for the others.
- `AssertUnreachable()`: Always fails. Used to mark code that should never be executed.
- `AssertIn(obj1, obj2)`: Fails if the parent of `obj1` is not `obj2`.

All assertions can take an additional argument holding a short explanation that will be shown if the assertion fails:

```
score = 0;
<Take treasure>;
Assert(score, 5, "The score increases by 5 after taking the treasure");
```

Yet another argument can be added in the comparison assertions (`Assert`, `AssertEqual`, etc.): a routine specifying how the given values will be shown when explaining a failure.

```
AssertEqual('a', 'a', "The character 'a' equals itself", AsChar);
```

Usually, the library will auto-detect the type and print accordingly, but in this case, Inform cannot differentiate between a number and a character, so we have to specify `AsChar`. The available routines are `AsNumber`, `AsClass`, `AsObject`, `AsString`, `AsRoutine`, `AsDictWord`, `AsChar`.

We can also add our own routine if needed:

```
[ AsStringWithSmiley x;
	print "~", (string) x, "~ :)";
];
```

## Roadmap

Things that I'd like to add to the library (with no promises):

- Assertions on strings (whether they match or not, whether one include another...)
- Hide game output during tests, and make it possible to capture the output from a test to make assertions on it.
