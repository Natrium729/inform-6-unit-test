! Below, the constant and the message have to be kept in sync!
Constant UNITTEST_VERSION = "0.1.0";
Message "Compiled with version 0.1.0 of Unit Test by Nathanaël Marion.";

#Ifndef DEBUG;
Message warning
	"Unit Test is being included without the DEBUG switch. If you're compiling your story for release, you should not include Unit Test!";
#Endif;

! Suppress warnings in the library when we are not developping it.
! (i.e. UNITTEST_DEV isn't defined.)
#Ifndef UNITTEST_DEV;
System_file;
#Endif;


! The number of tests run when running them all.
Global unittest_run_count = 0;
! The number of tests failed when running them.
Global unittest_failure_count = 0;
! The number of tests skipped when running them.
Global unittest_skip_count = 0;
! The test currently running.
Global unittest_current = 0;

! Concealed tests are not run when calling UnitTestRunAll.
#Ifndef concealed;
Attribute concealed;
#Endif;

! Tests that are run but fail are marked with this attribute.
#Ifndef failed;
Attribute failed;
#Endif;

! These `As*` routines are used for printing values when assertions fail. They are used in `_Repr` below.

[ AsNumber x;
	print x;
];

[ AsClass x;
	print "<", (name) x, ">";
];

[ AsObject x;
	print (name) x;
];

[ AsString x;
	print "<String ~", (string) x, "~>";
];

[ AsRoutine x;
	print "<Routine ", x, ">";
];

! This one is impossible to auto-detect on the Z-machine.
[ AsDictWord x;
	print "'", (address) x, "'";
];

! This one is impossible to auto-detect.
[ AsChar x;
	print "'", (char) x, "'";
];

[ _Repr x as_type;
	if (metaclass(as_type) == Routine) {
		as_type(x);
	} else {
		! We can only auto-detect dictionary words on Glulx.
		! `metaclass()` can't catch this case, so we do it manually.
		#Ifdef TARGET_GLULX;
		if (x->0 == $60) {
			AsDictWord(x);
			return;
		}
		#Endif;

		switch (metaclass(x)) {
			Class: AsClass(x);
			Object: AsObject(x);
			String: AsString(x);
			Routine: AsRoutine(x);
			default: print x;
		}
	}
];

! Prints an explanation for an assertion failure.
! Used by the assertion methods of UnitTest.
[ _PrintFailure desc text_start val1 text_middle val2 text_end as_type;
		! Print a new line and indent.
		print "^    ";
		! Show description if it is not 0.
		if (desc) {
			print (string) desc, ": ";
		}
		if (text_start) print (string) text_start;
		! Print the first value if `text_middle` is not false.
		! (So that the routine can be used with no value to print.)
		if (text_middle) {
			_Repr(val1, as_type);
			print (string) text_middle;
		} else {
			return;
		}
		! Print the second value if `text_end` is not false.
		! (So that the routine can be used with only 1 value to print.)
		if (text_end) {
			_Repr(val2, as_type);
			print (string) text_end;
		}
];

! Class used to create unit tests.
Class UnitTest
	with
		run [;
			unittest_current = self;
			give self ~failed;
			self.test();
			unittest_current = 0;
			return self hasnt failed;
		],

		test [;
			self._fail();
			print "^ERROR: ", (name) self, " has no test written.";
		],

		! Called when an assertion fails.
		! Prints the name of the test if it's the first of its failures.
		_fail [;
			if (self hasnt failed) {
				style bold;
				print "^", (name) self;
				style roman;
			}
			give self failed;
		],
;

! Runs all the unit tests.
! If `run_concealed` is true, concealed tests won't be skipped.
[ UnitTestRunAll run_concealed o;
	print "Running all unit tests...";

	unittest_run_count = 0;
	unittest_failure_count = 0;
	unittest_skip_count = 0;

	objectloop(o ofclass UnitTest) {
		if (o has concealed && (~~run_concealed)) {
			unittest_skip_count++;
			continue;
		}
		if (~~o.run()) unittest_failure_count++;
		unittest_run_count++;
	}

	print "^^";
	if (unittest_skip_count) {
		print "(", unittest_skip_count, " tests skipped.)^";
	}
	if (unittest_failure_count) {
		print unittest_run_count, " tests run, ", unittest_failure_count, " failed.";
	} else if (unittest_run_count) {
		print "All ", unittest_run_count, " tests passed!";
	} else {
		print "No test run!";
	}
	print "^^";
];

! Checks if an assertion is made outside of a test, and prints an error message if so.
[ IsAssertionOutside desc;
	if (unittest_current == 0) {
		print "ERROR: assertion ";
		if (desc) print "~", (string) desc, "~ ";
		"run outside of a test.^";
	}
	rfalse;
];


! And now, all the possible assertions.

[ Assert c desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (~~c) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", c, ", expected truthy value.", 0, 0, as_type);
	}
];

[ AssertEqual val expected desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val ~= expected) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val, ", expected ", expected, ".", as_type);
	}
];

[ AssertNotEqual val not_expected desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val == not_expected) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val, ", expected anything but ", not_expected, ".", as_type);
	}
];

[ AssertGreater val1 val2 desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val1 <= val2) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val1, ", expected greater than ", val2, ".", as_type);
	}
];

[ AssertGreaterOrEqual val1 val2 desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val1 < val2) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val1, ", expected at least ", val2, ".", as_type);
	}
];

[ AssertLess val1 val2 desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val1 >= val2) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val1, ", expected less than ", val2, ".", as_type);
	}
];

[ AssertLessOrEqual val1 val2 desc as_type;
	if (IsAssertionOutside(desc)) return;
	if (val1 > val2) {
		unittest_current._fail();
		_PrintFailure(desc, "Got ", val1, ", expected at most ", val2, ".", as_type);
	}
];

[ AssertUnreachable desc;
	if (IsAssertionOutside(desc)) return;
	unittest_current._fail();
	print "^    ";
	if (desc) print (string) desc, ": ";
	print "Executed code that should be unreachable.";
];

! Not strictly needed since we could write `AssertEqual(parent(obj1), obj2)`.
! But it's a useful shortcut and prints the current parent when failing.
[ AssertIn obj1 obj2 desc;
	if (IsAssertionOutside(desc)) return;
	if (~~ obj1 in obj2) {
		unittest_current._fail();
		print "^    ";
		if (desc) print (string) desc, ": ";
		print "Parent of ", (name) obj1, " is ", (name) parent(obj1), ", expected ", (name) obj2, ".";
	}
];
